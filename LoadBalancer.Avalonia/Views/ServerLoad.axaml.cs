using Avalonia.Controls;
using Avalonia.Markup.Xaml;
using LoadBalancer.Avalonia.ViewModels;

namespace LoadBalancer.Avalonia.Views
{
    public class ServerLoad : UserControl
    {
        public ServerLoad()
        {
            InitializeComponent();

            DataContext = new ServerLoadViewModel();
            ServerLoadViewModel.JobServer1 = this.FindControl<ProgressBar>("progressServerLoad1");
            ServerLoadViewModel.JobServer2 = this.FindControl<ProgressBar>("progressServerLoad2");
            ServerLoadViewModel.JobServer3 = this.FindControl<ProgressBar>("progressServerLoad3");
            ServerLoadViewModel.JobServer4 = this.FindControl<ProgressBar>("progressServerLoad4");
        }

        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
        }
    }
}
